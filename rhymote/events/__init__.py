from enum import Enum, auto


class EventType(Enum):
    COMPONENT_CONFIG_CHANGE = auto()
