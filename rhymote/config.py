# -*- Mode: python; coding: utf-8; tab-width: 4; indent-tabs-mode: t; -*-


class RhymoteConfig:
    # TODO: section for DataComponent (import dis)
    # - data_dir (filepath)

    # TODO: section for HTTPComponent
    # - enabled (bool)
    # - list of host/port/type/is_https interfaces, https (ipv4/ipv6 addresses), default should be 0.0.0.0 for all

    # TODO: section for RendezvouzComponent
    # - rendezvous points (list)

    def __init__(self, toml_path=None):
        # TODO: set defaults

        # TODO: handle custom TOML config file path
        self.toml_path = "$HOME/.local/share/rhythmbox/plugins/rhymote/config.toml"
        if toml_path:
            self.toml_path = "$HOME/.local/share/rhythmbox/plugins/rhymote/config.toml"

            # Use all sources

    def override_from_sources(self):
        self.override_from_toml(self.toml_path)
        self.override_from_env()
        self.override_from_gui()

    def override_from_toml(self, cfg_file: str = None):
        """
				Load values (overriding local state) configuration with values supplied by a configuration file
				"""
        # TODO: check if the config has changed at all, if not return None
        raise NotImplementedError

    def override_from_env(self):
        """
				Override the configuration with values supplied by ENV
				"""
        # TODO: check if the config has changed at all, if not return None
        raise NotImplementedError

    def override_from_gui(self, guiState=None):
        """
				Override the configuration with values supplied by GUI.

				The GUI *should* be the final source of truth
				"""
        # TODO: check if the config has changed at all, if not return None
        raise NotImplementedError

    def is_valid():
        """
				Check if this configuration is valid
				"""
        # TODO: maybe return multiple values here -- make list of validations to run as async tasks
        # and report progress as they run/complete
        raise NotImplementedError

    def __str__(self):
        return str(self.__dict__)

    def __eq__(self, other):
        return self.__dict__ == other.__dict__


class RBComponentConfig:
    pass


class RemoteComponentConfig:
    pass


DEFAULT_PORT: int = 5678


class HttpComponentConfig:
    def __init__(self):
        self.port = DEFAULT_PORT
