from abc import ABC, abstractmethod
from enum import Enum, auto


class CommandType(Enum):
    COMPONENT_STOP = auto()

    PLAYBACK_PLAY_PAUSE = auto()
    PLAYBACK_STOP = auto()
    PLAYBACK_NEXT = auto()
    PLAYBACK_PREVIOUS = auto()

    HTTP_SERVER_START = auto()
    HTTP_SERVER_STOP = auto()


class Command(ABC):
    @abstractmethod
    def cmd_type(self):
        self.cmd_type


class ComponentStop(Command):
    def cmd_type(self):
        return CommandType.COMPONENT_STOP


class PlaybackPlayPause(Command):
    def cmd_type(self):
        return CommandType.PLAYBACK_PLAY_PAUSE


class PlaybackStop(Command):
    def cmd_type(self):
        return CommandType.PLAYBACK_STOP


class PlaybackNext(Command):
    def cmd_type(self):
        return CommandType.PLAYBACK_NEXT


class PlaybackPrevious(Command):
    def cmd_type(self):
        return CommandType.PLAYBACK_PREVIOUS


class HTTPServerStart(Command):
    def cmd_type(self):
        return CommandType.HTTP_SERVER_START


class HTTPServerStop(Command):
    def cmd_type(self):
        return CommandType.HTTP_SERVER_STOP
