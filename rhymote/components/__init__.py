import logging

from gi.repository import Gdk, GLib
from rhymote.events import EventType
from rhymote.commands import CommandType, Command, ComponentStop
from rhymote.errors import InvalidStateError
from abc import ABC, abstractmethod
from asyncio import Queue
from typing import Optional, Callable
from asyncio import Queue, QueueEmpty

DEFAULT_LOGGING_FORMAT: str = "[%(asctime)-15s] [%(name)s] [%(levelname)s] %(message)s"
DEFAULT_LOG_LEVEL: int = logging.INFO


class Component(ABC):
    def log_level() -> int:
        return logging.INFO

    def __init__(self, parent_logger=None):
        """
        Initialize a component
        """

        self.commands: Queue = Queue()
        self.events: Queue = Queue()
        self.event_handlers: Dict[EventType, Dict[str, Callable]] = {}
        self.thread: Optional[Thread] = None
        self.running: Boolean = False
        self.read_messages_timeout_seconds: int = 1

        self.logger: Optional[logging.Logger] = None
        self.log_level: Optional[int] = None
        self.logging_format: Optional[str] = None

        self.setup_logger(parent_logger)

    def setup_logger(self, parent_logger: Optional[logging.Logger] = None):
        """
        Setup the logger

        """
        # if a logger has already been setup exit early
        if self.logger is not None:
            return

        # Set logging defaults
        if self.logging_format is None:
            self.logging_format: str = DEFAULT_LOGGING_FORMAT

        if self.log_level is None:
            self.log_level: int = DEFAULT_LOG_LEVEL

        if parent_logger is not None:
            self.logger = parent_logger.getChild(self.name())
            self.logger.propagate = False
        else:
            self.logger = logging.getLogger(self.name())

        handler = logging.StreamHandler()
        formatter = logging.Formatter(self.logging_format)
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)
        self.logger.setLevel(self.log_level)

    @abstractmethod
    def name(self) -> str:
        raise NotImplementedError

    def start(self):
        """
        Start running this component in a thread
        GTK should automagically pick it up: https://wiki.gnome.org/Projects/PyGObject/Threading
        """
        if self.commands is None:
            raise InvalidStateError

        self.logger.info("starting...")
        self.running = True

        def step():
            try:
                # Process all available messages
                while True:
                    cmd: Command = self.commands.get_nowait()

                    # Handle shutdown
                    if cmd.cmd_type() == CommandType.COMPONENT_STOP:
                        self.logger.info(
                            "received component shutdown command"
                        )  # TODO: make debug
                        self.running = False
                        return False  # GLib won't reschedule

                    # handle individual command
                    self.logger.debug("handling command!")
                    self.handle_command(cmd)

            except QueueEmpty:
                return True

        Gdk.threads_add_timeout_seconds(
            GLib.PRIORITY_DEFAULT_IDLE, self.read_messages_timeout_seconds, step
        )  # TODO: FIX, does it way too much

        self.logger.info(
            "started, listening for commands every %s seconds"
            % self.read_messages_timeout_seconds
        )

    def stop(self):
        """
        Stop running the component
        """
        if not self.running:
            raise InvalidStateError("component is not running")

        self.logger.info("stopping...")
        self.send_command(ComponentStop())

    def add_event_handler(
        self,
        eventType: EventType,
        cb: Callable,
        name: Optional[str] = None,
        replace_existing=True,
    ) -> str:
        # TODO: what does stop look like integrating with GIO event loop?
        raise NotImplementedError

    def send_command(self, command: Command):
        """
        Send a command to the component
        """
        self.commands.put_nowait(command)
