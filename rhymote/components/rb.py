import logging

from gi.repository.RB import Shell, RhythmDB
from . import Component
from rhymote.components.config import ConfigComponent
from rhymote.config import RBComponentConfig
from rhymote.commands import (
    Command,
    PlaybackPlayPause,
    PlaybackStop,
    PlaybackNext,
    PlaybackPrevious,
)
from typing import Optional


class RBComponent(Component):
    """
    RBComponent performs the Rythmbox-specific functionality for the system.
    All communication with rhythmbox should go through one or more instances of this component.

    Attributes:
        shell  A gi.repository.RB.Shell
    """

    def __init__(
        self,
        shell: Shell,
        initial_config: RBComponentConfig,
        config_component: ConfigComponent,
        parent_logger: logging.Logger = None,
    ):
        super().__init__(parent_logger=parent_logger)

        self.config = initial_config
        self.shell: Optional[Shell] = shell
        self.db: Optional[RhythmDB] = shell.props.db

        if self.shell is None:
            raise InvalidStateError("shell is required")

    def name(self) -> str:
        return "rb"

    def handle_command(self, cmd: Command):
        # Ensure shell is present
        if self.shell is None:
            self.logger.error("shell missing")
            raise InvalidStateError("shell not provided to RBComponent")

        # Handle command
        if isinstance(cmd, PlaybackPlayPause):
            self.logger.debug("received PlayPause")
            self.shell.props.shell_player.playpause()
        elif isinstance(cmd, PlaybackStop):
            self.logger.debug("received StopPlayback")
            self.shell.props.shell_player.stop()
        elif isinstance(cmd, PlaybackNext):
            self.logger.debug("received PlayNext")
            self.shell.props.shell_player.do_next()
        elif isinstance(cmd, PlaybackPrevious):
            self.logger.debug("received PlayPrevious")
            self.shell.props.shell_player.do_previous()
        else:
            self.logger.warn("received unrecognized command")
