import json
import rhymote.commands
import gi

gi.require_version("Soup", "2.4")
from gi.repository import Soup

from . import Component
from logging import Logger
from rhymote.errors import InvalidStateError
from rhymote.config import HttpComponentConfig
from rhymote.components.rb import RBComponent
from rhymote.components.config import ConfigComponent
from typing import Optional


class HttpComponent(Component):
    def __init__(
        self,
        initial_config: HttpComponentConfig,
        config_component: ConfigComponent,
        rb_component: RBComponent,
        parent_logger: Logger = None,
    ):
        super().__init__(parent_logger=parent_logger)

        self.config = initial_config
        self.http_server: Optional[Soup.Server] = None
        self.http_server_uris: List[str] = []
        self.ws_connections = []

        self.simple_command_cbs = {
            "/commands/playback-play-pause": lambda: rb_component.send_command(
                rhymote.commands.PlaybackPlayPause()
            ),
            "/commands/playback-stop": lambda: rb_component.send_command(
                rhymote.commands.PlaybackStop()
            ),
            "/commands/playback-next": lambda: rb_component.send_command(
                rhymote.commands.PlaybackNext()
            ),
            "/commands/playback-previous": lambda: rb_component.send_command(
                rhymote.commands.PlaybackPrevious()
            ),
        }

        self.configure_http_server()

    def name(self) -> str:
        return "http"

    def configure_http_server(self):
        """
        Configure the internal HTTP server
        """
        self.logger.info("configuring HTTP server")
        if self.http_server is not None:
            raise InvalidStateError("HTTP Server has already been configured")

        self.http_server = Soup.Server()

        self.http_server.add_handler("/", callback=self.handle_root)

        for path in self.simple_command_cbs:
            self.http_server.add_handler(path, callback=self.handle_simple_path_command)

    def handle_simple_path_command(self, server, msg, path, query, client):
        """
        POST /commands
        Handler for receiveing command over HTTP
        """

        # Ensure only POST makes it through (for now)
        if msg.method != "POST":
            msg.set_status(400)
            self.logger.warning("non-POST simple path command received")
            return

        # Handle only simple commands for now
        if path not in self.simple_command_cbs:
            msg.set_status(400)
            self.logger.warning("non-POST simple path command received")
            return

        self.simple_command_cbs[path]()

    def get_state(self):
        """
        Get the state of the HTTP server (normally served from root)
        """
        return {"running": True}

    def handle_root(self, server, msg, path, query, client):
        # Ensure only GET makes it through (for now)
        if msg.method != "GET":
            msg.set_status(400)
            self.logger.warning("non-GET simple path command received")
            return

        msg.set_status(200)
        msg.set_response(
            "application/json",
            Soup.MemoryUse.TEMPORARY,
            json.dumps(self.get_state(), ensure_ascii=False).encode(),
        )

    def server_start(self):
        """
        Start the HTTP server
        """
        if self.http_server is None:
            raise InvalidStateError("HTTP Server has already been configured")

        self.http_server.listen_all(self.config.port, 0)
        self.http_server_uris = self.http_server.get_uris()

        self.logger.info(
            "HTTP server listening at:\n%s"
            % "\n".join(uri.to_string(False) for uri in self.http_server_uris)
        )

        self.http_server.run_async()

    def server_stop(self):
        """
        Stop the HTTP server
        """
        self.http_server.quit()
        self.logger.info("HTTP server stopped")
        pass

    def handle_command(self, cmd: rhymote.commands.Command):
        # Handle command
        if isinstance(cmd, rhymote.commands.HTTPServerStart):
            self.logger.debug("received StartHTTPServer")
            self.server_start()
        elif isinstance(cmd, rhymote.commands.HTTPServerStop):
            self.logger.debug("received StartHTTPServer")
            self.server_stop()
        else:
            self.logger.warn("received unrecognized command", cmd)
