import logging

from . import Component
from rhymote.config import RhymoteConfig
from rhymote.commands import Command
from typing import Optional


class ConfigComponent(Component):
    def __init__(
        self, config: RhymoteConfig = None, parent_logger: logging.Logger = None
    ):
        super().__init__(parent_logger=parent_logger)

        # TODO: attempt parse/pull config at launch time?

        self.config = config

    def name(self) -> str:
        return "config"

    def handle_command(self, cmd: Command):
        # TODO: handle config update commands
        raise NotImplementedError
