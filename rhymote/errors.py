class InvalidStateError(Exception):
    """Raised when an operation is attempted on an object with invalid state

    Attributes:
        message -- explanation of what/why state was invalid
    """

    def __init__(self, message):
        self.message = message
