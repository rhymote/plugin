# -*- Mode: python; coding: utf-8; tab-width: 4; indent-tabs-mode: t; -*-

import gi

gi.require_version("Peas", "1.0")
gi.require_version("RB", "3.0")
from gi.repository import GLib, GObject, RB, Peas

import logging
import json
import gettext

from rhymote.config import HttpComponentConfig, RBComponentConfig
from rhymote.commands import HTTPServerStart
from rhymote.components.config import ConfigComponent
from rhymote.components.rb import RBComponent
from rhymote.components.http import HttpComponent
from typing import Optional, Any

gettext.install("rhythmbox", RB.locale_dir())

DEFAULT_APP_LOGGING_FORMAT: str = "[%(asctime)-15s] [%(name)s] [%(levelname)s] %(message)s"
DEFAULT_APP_LOG_LEVEL: int = logging.INFO


class RhymotePlugin(GObject.Object, Peas.Activatable):
    object = GObject.Property(type=GObject.Object)

    def __init__(self):
        """
		Plugin initialization
		"""
        super(RhymotePlugin, self).__init__()

        self.logger: Optional[logging.Logger] = None
        self.components: Dict[str, Component] = {}

        self.setup_logger()

        # TODO: this should maybe be moved into something else, it duplicated component code
        # maybe the top level Rhymote component?

    def setup_logger(self):
        """
		Setup local logger
		"""
        # Only setup the logger if it's not already setup
        if self.logger is not None:
            return

        self.logging_format: str = DEFAULT_APP_LOGGING_FORMAT
        self.log_level: int = DEFAULT_APP_LOG_LEVEL

        self.logger = logging.getLogger("rhymote")
        handler = logging.StreamHandler()
        formatter = logging.Formatter(self.logging_format)
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)
        self.logger.setLevel(self.log_level)

    def do_activate(self):
        """
		Plugin activation
		"""
        self.logger.info("starting plugin activation")

        if self.object is None:
            self.logger.error("GObject not provided, aborting activation")
            return

        config_component = ConfigComponent(parent_logger=self.logger)
        config_component.start()
        self.components[config_component.name()] = config_component

        rb_component = RBComponent(
            self.object,
            RBComponentConfig(),
            config_component=config_component,
            parent_logger=self.logger,
        )
        rb_component.start()
        self.components[rb_component.name()] = rb_component

        http_component = HttpComponent(
            HttpComponentConfig(),
            config_component=config_component,
            rb_component=rb_component,
            parent_logger=self.logger,
        )
        http_component.start()
        self.components[http_component.name()] = http_component

        # Start the HTTP server
        http_component.send_command(HTTPServerStart())

    def do_deactivate(self):
        """
		Plugin deactivation
		"""
        self.logger.info("starting plugin deactivation")

        self.logger = None
        self.components = None
