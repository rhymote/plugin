# Connections #

Unfortunately, enabling two computers to communicate reliably over the internet is still a non-trivial feat. The complexity of connection negotiation over various mediums and through various proxies is encapsulated by the various `Connection` classes (which are managed by `rhymote`'s `ConnectionComponent`). With this model, `rhymote` is able to extensibly support various different kinds of connections.

## Interface ##

TODO

# Connection - init(settingsobj), send(obj), receive(obj), get_stats(), teardown()

# RhymoteServerConnection #

send/receive data and commands with a remote rhmote server, point it @ rhymote server
connections can be remote or local -- this means that you can start a Rymote server on your own infrastrucutre, or inside Rhythmbox itself, and all you have to do is point `rhymote` at the server.

## Connection logic state machine ##

TODO

## Automated negotiation/upgrading ##

TODO, more detail on automated negotiation/upgrading of the server

# DBUSConnection #

## Connection logic state machine ##

TODO

# IPFSConnection #

TODO, send/receive data and commands with IPFS

## Connection logic state machine ##

TODO

# DatConnection #

TODO, send/receive data with Dat

## Connection logic state machine ##

TODO

# WebRTCConnection #

TODO, used in/created by the connection_component (also makes a websocket_component? also support SSE!)

## Connection logic state machine ##

TODO

# BluetoothConnection #


TODO, used in/created by the connection_component (also makes a websocket_component?)

## Connection logic state machine ##

TODO
