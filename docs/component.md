# Components #

`rhymote` `Component`s expose a consistent interface to enable easy & consistent interaction.

## Interface ##

```python
class Component():
    start()

    stop()

    add_event_handler()

    send_command()
```

Components must be fully asynchronous with use of Python 3's [`asyncio`](https://docs.python.org/3/library/asyncio.html).

All components that interact inside of `rhymote` adhere to this interface. Creation of `Component`s are handled by their respective pythonic `__init__` and `__del__` methods.

## Component Listing ##

### ConfigComponent ###

The `ConfigComponent` initializes, manages and enables dynamic configuration of `rhymote`. When `rhymote` first starts up, it creates `ConfigComponent` which pulls configuration from wherever is necessary. When configuration to `rhymote` needs to change, messages need to be sent to the `ConfigComponent`, and the `ConfigComponent` will forward the updated changes (if successful) to other components by publishing `Event`s.

For more information, see `components/config.md`.

### RBComponent ###

The `RBComponent` performs all functionality that actually requires `rhythmbox` integration. This includes (but is not limited to) playing files, skipping, looking up playlist information.

For more information, see `components/rb.md`.

### DataComponent ###

The `DataComponent` manages local data storage/exporting, including but not limited to local file caching, chunking, and plugin state management, including a long-lived `EntityStore` (backed by [SQLite](https://www.sqlite.org)) to ensure that disconnected `Remote`s are able to re-connect.

For more information, see `components/data.md`.

### RemoteComponent ###

The `RemoteComponent` orchestrates other components at a higher level in order to provide the command interface for `rhymote` itself, acting as a central interaction point for the plugin code (i.e. `RhymotePlugin`) itself.

For more information, see `components/remote.md`

### HTTPComponent ###

The `HTTPComponent` makes the `RemoteComponent` available over HTTP. This component is powered by [GNOME's `Soup` library](https://lazka.github.io/pgi-docs/#Soup-2.4), piggybacking on `GObject` and `GLib`'s mainloop in order to run an HTTP server that exposes `RemoteComponent`. While the `HTTPComponent` is *not* protected by [RBAC](https://en.wikipedia.org/wiki/Role-based_access_control) or other such [AuthN](https://en.wikipedia.org/wiki/Authentication)/[AuthZ](https://en.wikipedia.org/wiki/Authorization) schemes, out-of-process clients that connect *are* subjected to  -- but communication to it is AES encrypted with a key generated at time of `Remote` registration.

For more information, see `components/http.md`.

### RendezvousComponent ###

The `RendezvousComponent` manages the methods and integrations by which remotes (ex. a mobile phone in your control) can find and communicate with the local `rhymote` plugin. The `RendezvousComponent` provides an abstraction over the many ways this plugin (and the embedded command-processing `RBComponent`) can speak to one or more clients over one or more channels.

The `RendezvousComponent` is not needed in the case of a same-machine or local network connection -- If the `HTTPComponent` is enabled, it will be listening on the expected places. The `RendezvousComponent` is handy when dealing with more exotic connection options, such as through a server that is running a Rhymote server rendezvous/proxy (see `rhymote/server`). The `HTTPComponent` does not provide rendezvous/proxying capability, because it is only meant to be accessed locally.

For more information, see `components/rendezvous.md`.
