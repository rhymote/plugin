# Architecture #

This document lays out the architecture of the `rhymote` plugin.

In general, the `rhymote` plugin functionality is a system of interacting `Component`s which receive `Command`s and emit `Event`s to interested parties.

**Components** (see `component.md` for more information)
**Commands** (see `command.md` for more information)
**Events** (see `event.md` for more information)

## Diagram ##

TODO
