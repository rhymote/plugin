from gi.repository import GObject, PeasGtk

## TODO: FIX ##


class WebRemoteConfig(GObject.Object, PeasGtk.Configurable):
    __gtype_name__ = "WebRemoteConfig"
    object = GObject.property(type=GObject.Object)

    def accesskey_focus_out_cb(self, widget, event):
        k = widget.get_text()
        if k != self.access_key:
            print("changing access key to %s" % k)
            self.access_key = k
            self.settings["access-key"] = k

            return False

    def update_port(self):
        hostname = get_host_name()
        port = self.settings["listen-port"]
        url = "http://%s:%d/" % (hostname, port)
        label = _("Launch web remote control")
        self.launch_link.set_markup('<a href="%s">%s</a>' % (url, label))

        self.portnumber.set_text("%d" % port)

        def settings_changed_cb(self, settings, key):
            if key == "listen-port":
                self.update_port()

    def do_create_configure_widget(self):
        self.settings = Gio.Settings.new("org.gnome.rhythmbox.plugins.webremote")
        self.settings.connect("changed", self.settings_changed_cb)

        ui_file = rb.find_plugin_file(self, "webremote-config.ui")
        self.builder = Gtk.Builder()
        self.builder.add_from_file(ui_file)

        content = self.builder.get_object("webremote-config")

        self.portnumber = self.builder.get_object("portnumber")
        self.launch_link = self.builder.get_object("launch-link")
        self.update_port()

        self.key_entry = self.builder.get_object("accesskey")
        self.access_key = self.settings["access-key"]
        if self.access_key:
            self.key_entry.set_text(self.access_key)
        self.key_entry.connect("focus-out-event", self.accesskey_focus_out_cb)

        return content


GObject.type_register(WebRemoteConfig)
