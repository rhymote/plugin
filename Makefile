.PHONY: all install setup build lint clean \
				check-tool-pipenv \
				local-rhythmbox-plugin rhythmbox-dev

all: build install

NAME=rhymote
VERSION=1.0

PIPENV=pipenv
RHYTHMBOX=rhythmbox

MAKEFILE_DIR:=$(strip $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST)))))

RHYTHMBOX_LOCAL_PLUGINS_DIR=$(HOME)/.local/share/rhythmbox/plugins

TARGET_DIR=$(MAKEFILE_DIR)/target
TARGET_ZIP_NAME=rhymote-$(VERSION).zip
TARGET_ZIP_PATH=$(TARGET_DIR)/$(TARGET_ZIP_NAME)

check-tool-pipenv:
ifeq (, $(PIPENV))
		$(error "`pipenv` doesn't seem to be installed (https://pipenv.readthedocs.io/en/latest)")
endif

install: check-tool-pipenv
	$(PIPENV) install

local-rhythmbox-plugin:
	@echo -e "[info] installing plugin @ [$(MAKEFILE_DIR)] to local plugin directory [$(RHYTHMBOX_LOCAL_PLUGINS_DIR)]"
	mkdir -p $(RHYTHMBOX_LOCAL_PLUGINS_DIR) || cd $(RHYTHMBOX_LOCAL_PLUGINS_DIR) && ln -s $(MAKEFILE_DIR)

setup: install local-rhythmbox-plugin

lint: check-tool-pipenv
	$(PIPENV) run black .

clean:
	$(PIPENV) clean

build: setup
	@echo -e "[info] Rhythmbox plugin ZIP file saved to [$(TARGET_ZIP_PATH)]"

rhythmbox-dev:
	$(RHYTHMBOX) -D $(NAME)

rhythmbox-dev-watch:
	find . -name "*.py"	| entr -r $(RHYTHMBOX) -D $(NAME)
