# Rhythmbox plugin for Rhymote #

[`rhythmbox`](https://wiki.gnome.org/Apps/Rhythmbox)-internal plugin that allows remote manipulation from `rhymote` clients.

The code in this repository is possible by standing on the shoulders of the following giants:

- [Gnome](https://wiki.gnome.org)
- [Rhythmbox](https://wiki.gnome.org/Apps/Rhythmbox)
- [existing `webremote` plugin for rythmbox](https://github.com/GNOME/rhythmbox/blob/master/plugins/webremote)

# Requirements #

1. [`pip`](https://pip.pypa.io/en/stable/installing/)
2. [`pipenv`](https://pipenv.readthedocs.io/en/latest/install/#installing-pipenv)

# Installing Rhymote #

1. Download a release of `rhymote`
2. Unpack it to a place where `rhythmbox` looks for plugins (`$HOME/.local/share/rhythmbox/plugins`)

# Building Rhymote #

```
$ pip install --user pipenv
$ git clone <this repository>
$ cd rhymote
$ make setup # alternatively, `pipenv install`
```

If you'd like to install the built library to your local `rhythmbox` for testing, run `make local-rhythmbox-plugin`.

# Features #

## Current ##

- [x] Basic player control (Play/Pause, Stop, Skip, Previous)
- [x] Basic access over HTTP (insecure)

Feature parity may differ depending on *how* Rhymote is connected and the limitations of that connection.

## Upcoming/WIP/TODO ##

This project is far from complete, but here's a listing of planned future features in order of relative importance (to functionality):

- **TODO** Security all over (Bearer Token auth + preshared AES key should be enough)
- **TODO** SSE enabled endpoint that sends information (this might involve extending Soup.Server in another library possibily)
- **TODO** Build out proper configuration parsing
- **TODO** Create & start `DataComponent` to manage on-disk cache of transient data pertinent to the application or Rhythmbox
- **TODO** Use `DataComponent` to get more information out of Rhythmbox (starting with track information)
- **TODO** Create & start `RendezvousComponent` to manage meeting clients (rendezvous) and figuring out best connection method
- **TODO** More functionality/commands for `RBComponent`/`DataComponent` (ex. `GET_MUSIC_FILE`, `GET_ALBUM`, `GET_LIBRARY_STATS`, etc)
- **TODO** Rhythmbox-native GUI for settings (ensure in-app GUI can write default configuration out to a file)
- **TODO** GUI & functionality for QR code support w/ `RendezvousComponent` (scan a QR code on a phone and the phone will try multiple ways to connect)
